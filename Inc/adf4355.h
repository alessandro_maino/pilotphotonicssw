/*
 * ADF4355.h
 *
 *  Created on: 2 Mar 2018
 *      Author: Cilluffo Antonino
 */

#ifndef ADF4355_H_
#define ADF4355_H_

#include "stm32f1xx_hal_spi.h"

SPI_HandleTypeDef hspi2;
void ad5144_write( uint8_t cs, uint8_t pot_index, uint8_t address, uint8_t value);
void parse_hex(char* hex_string, uint8_t* hex_bytes);
uint16_t ad5144_read( uint8_t cs, uint8_t pot_index, uint8_t address, uint8_t dev);
int set_frequency_khz(uint32_t frequency);

#endif /* ADF4355_H_ */
