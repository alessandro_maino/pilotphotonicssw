/*
 * management.h
 *
 *  Created on: 5 Mar 2018
 *      Author: Cilluffo Antonino
 */

#ifndef MANAGEMENT_H_
#define MANAGEMENT_H_

#define structpacked struct __attribute__((__packed__))

#define END_FLAG 0x0a

void managerTask(void const * argument);

typedef structpacked _DATAVAL {
    union  {
        char      s8val;
        short     s16val;
        int       s32val;
        uint8_t   u8val;
        uint16_t  u16val;
        uint32_t  u32val;
        uint8_t*  data;
    };
}DATAVAL;


typedef structpacked _MESSAGE {
	uint16_t msg_len;
	uint16_t seq_num;
	uint8_t  op_code;
	uint8_t  object_addr;
	uint8_t  data_type;
	uint16_t data_len;
	DATAVAL  data_val;
	uint32_t  csum;
	uint8_t  end_flag;

}MESSAGE;


typedef enum {
    INT32,
    INT16,
    INT8,
    UINT32,
    UINT16,
    UINT8,
    STRING,
    DATA,
    NULL_TYPE

} DataType;

typedef enum {
	SET_MSG, GET_MSG, MSG_ERROR, MSG_NOERROR, SET_RESPONSE, GET_RESPONSE
} msgType;




#endif /* MANAGEMENT_H_ */
