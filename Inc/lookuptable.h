/*
 * lookuptable.h
 *
 *  Created on: 15 May 2018
 *      Author: Cilluffo Antonino
 */

#ifndef LOOKUPTABLE_H_
#define LOOKUPTABLE_H_

#include "stdint.h"

#define structpacked struct __attribute__((__packed__))

typedef structpacked _LOOK_UP_TABLE {

	uint16_t  vcs_bend_rdac1_P1;
	uint16_t  pwr_rf_cntl_rdac2_P1;
	uint16_t  vc_master_dbr_rdac4_P1;
	uint16_t  vc_master_gain_rdac1_P2;
	uint16_t  ldo_cntl_rdac3_P2;
	uint16_t  vc_slave_dbr_rdac4_P2;
	uint16_t  vc_laser2_dbr_rda1_P3;
	uint16_t  vc_laser1_dbr_rda3_P3;
	uint16_t  vc_laser1_gain_rda2_P3;
	uint16_t  vc_laser2_gain_rda4_P3;
	uint16_t  vc_laser3_gain_rda1_P4;
	uint16_t  vc_laser4_gain_rda3_P4;
	uint16_t  vc_laser4_dbr_rda2_P4;
	uint16_t  vc_laser3_dbr_rda4_P4;
	uint16_t  dummy[60];

}LOOK_UP_TABLE;

extern const LOOK_UP_TABLE lookup_table;

#endif /* LOOKUPTABLE_H_ */

