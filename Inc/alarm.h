/*
 * alarm.h
 *
 *  Created on: 8 Mar 2018
 *      Author: Cilluffo Antonino
 */

#ifndef ALARM_H_
#define ALARM_H_

#define LO_ALM_DEFECT						1
#define TEC_TEMP_ALARM_DEFECT				2

void LowSpeedManagerTask(void const * argument);
void read_adc1();
#endif /* ALARM_H_ */
