/*
 * decoder.h
 *
 *  Created on: 8 Mar 2018
 *      Author: Cilluffo Antonino
 */

#ifndef DECODER_H_
#define DECODER_H_

#include "management.h"

int decoder(MESSAGE* tx_msg, MESSAGE* rx_msg);
uint32_t checksum(MESSAGE *message);

#define RDAC1_ADDR					0x00
#define RDAC2_ADDR					0x01
#define RDAC3_ADDR					0x02
#define RDAC4_ADDR					0x03
#define CHAGE_RES					0x10
#define CTRL_REG					0xD0
#define RDAC_INC_CMD				0x50
#define LOAD_INP_REG_CMD			0x20
#define COPY_INP_REG_TO_RDAC_CMD	0x60
#define WRITE_TO_RDAC_CMD			0x10
#define READ_BACK_TO_RDAC_CMD		0x30

typedef enum obj {
	I_LASER1_GAIN,
	I_LASER2_GAIN,
	I_LASER3_GAIN,
	I_LASER4_GAIN,
	I_LASER1_DBR,
	I_LASER2_DBR,
	I_LASER3_DBR,
	I_LASER4_DBR,
	TEC_TEMP,
	PWR_DET,
	VCS_BEND_RDAC1_P1,
	PWR_RF_CNTL_RDAC2_P1,
	VC_MASTER_DBR_RDAC4_P1,
	VC_MASTER_GAIN_RDAC1_P2,
	LDO_CNTL_RDAC3_P2,
	VC_SLAVE_DBR_RDAC4_P2,
	VC_LASER2_DBR_RDAC1_P3,
	VC_LASER1_DBR_RDAC3_P3,
	VC_LASER1_GAIN_RDAC2_P3,
	VC_LASER2_GAIN_RDAC4_P3,
	VC_LASER3_GAIN_RDAC1_P4,
	VC_LASER4_GAIN_RDAC3_P4,
	VC_LASER4_DBR_RDAC2_P4,
	VC_LASER3_DBR_RDAC4_P4,
	LOOKUP_TABLE,
	ALARMS_BITMAP,
	I_MASTER_GAIN,
	I_SLAVE_DBR,
	I_MASTER_DBR,
	I_S_BEND,
	PLL_FREQUENCY
}obj;

#endif /* DECODER_H_ */
