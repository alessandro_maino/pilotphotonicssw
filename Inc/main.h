/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define I_LASER2_GAIN_Pin GPIO_PIN_0
#define I_LASER2_GAIN_GPIO_Port GPIOC
#define I_LASER1_GAIN_Pin GPIO_PIN_1
#define I_LASER1_GAIN_GPIO_Port GPIOC
#define I_LASER1_DBR_Pin GPIO_PIN_2
#define I_LASER1_DBR_GPIO_Port GPIOC
#define I_LASER2_DBR_Pin GPIO_PIN_3
#define I_LASER2_DBR_GPIO_Port GPIOC
#define I_MASTER_GAIN_Pin GPIO_PIN_0
#define I_MASTER_GAIN_GPIO_Port GPIOA
#define I_SLAVE_DBR_Pin GPIO_PIN_1
#define I_SLAVE_DBR_GPIO_Port GPIOA
#define PWR_DECT_Pin GPIO_PIN_2
#define PWR_DECT_GPIO_Port GPIOA
#define I_MASTER_DBR_Pin GPIO_PIN_3
#define I_MASTER_DBR_GPIO_Port GPIOA
#define I_S_BEND_Pin GPIO_PIN_4
#define I_S_BEND_GPIO_Port GPIOA
#define I_LASER3_DBR_Pin GPIO_PIN_5
#define I_LASER3_DBR_GPIO_Port GPIOA
#define I_LASER4_DBR_Pin GPIO_PIN_6
#define I_LASER4_DBR_GPIO_Port GPIOA
#define I_LASER4_GAIN_Pin GPIO_PIN_7
#define I_LASER4_GAIN_GPIO_Port GPIOA
#define I_LASER3_GAIN_Pin GPIO_PIN_4
#define I_LASER3_GAIN_GPIO_Port GPIOC
#define TEC_TEMP_ALARM_Pin GPIO_PIN_5
#define TEC_TEMP_ALARM_GPIO_Port GPIOC
#define F_REF_EX_Pin GPIO_PIN_0
#define F_REF_EX_GPIO_Port GPIOB
#define LO_ALM_Pin GPIO_PIN_1
#define LO_ALM_GPIO_Port GPIOB
#define I_LIM_MASTER_DBR_Pin GPIO_PIN_10
#define I_LIM_MASTER_DBR_GPIO_Port GPIOB
#define I_LIM_S_BEND_Pin GPIO_PIN_11
#define I_LIM_S_BEND_GPIO_Port GPIOB
#define SPI2_CS_LO_Pin GPIO_PIN_12
#define SPI2_CS_LO_GPIO_Port GPIOB
#define SPI2_CS_POTE_1_Pin GPIO_PIN_6
#define SPI2_CS_POTE_1_GPIO_Port GPIOC
#define SPI2_CS_POTE_2_Pin GPIO_PIN_7
#define SPI2_CS_POTE_2_GPIO_Port GPIOC
#define SPI2_CS_POTE_3_Pin GPIO_PIN_8
#define SPI2_CS_POTE_3_GPIO_Port GPIOC
#define SPI2_CS_POTE_4_Pin GPIO_PIN_9
#define SPI2_CS_POTE_4_GPIO_Port GPIOC
#define I_LIM_LASER3_GAIN_Pin GPIO_PIN_8
#define I_LIM_LASER3_GAIN_GPIO_Port GPIOA
#define I_LIM_LASER4_GAIN_Pin GPIO_PIN_11
#define I_LIM_LASER4_GAIN_GPIO_Port GPIOA
#define I_LIM_LASER4_DBR_Pin GPIO_PIN_12
#define I_LIM_LASER4_DBR_GPIO_Port GPIOA
#define I_LIM_LASER3_DBR_Pin GPIO_PIN_12
#define I_LIM_LASER3_DBR_GPIO_Port GPIOC
#define I_LIM_MASTER_GAIN_Pin GPIO_PIN_2
#define I_LIM_MASTER_GAIN_GPIO_Port GPIOD
#define I_LIM_SLAVE_DBR_Pin GPIO_PIN_5
#define I_LIM_SLAVE_DBR_GPIO_Port GPIOB
#define I_LIM_LASER2_DBR_Pin GPIO_PIN_6
#define I_LIM_LASER2_DBR_GPIO_Port GPIOB
#define I_LIM_LASER1_DBR_Pin GPIO_PIN_7
#define I_LIM_LASER1_DBR_GPIO_Port GPIOB
#define I_LIM_LASER1_GAIN_Pin GPIO_PIN_8
#define I_LIM_LASER1_GAIN_GPIO_Port GPIOB
#define I_LIM_LASER2_GAIN_Pin GPIO_PIN_9
#define I_LIM_LASER2_GAIN_GPIO_Port GPIOB
#define I_MASTER_GAIN_ADC		0
#define I_SLAVE_DBR_ADC			1
#define PWR_DECT_ADC			2
#define I_MASTER_DBR_ADC		3
#define I_S_BEND_ADC			4
#define I_LASER3_DBR_ADC		5
#define I_LASER4_DBR_ADC		6
#define I_LASER4_GAIN_ADC		7
#define I_LASER2_GAIN_ADC		8
#define I_LASER1_GAIN_ADC		9
#define I_LASER1_DBR_ADC		10
#define I_LASER2_DBR_ADC		11
#define I_LASER3_GAIN_ADC		12
#define TEC_TEMP_ALARM_ADC		13



/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
 #define USE_FULL_ASSERT    1U 

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
