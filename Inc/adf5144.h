/*
 * AD5144.h
 *
 *  Created on: 5 Mar 2018
 *      Author: Cilluffo Antonino
 */

#ifndef ADF5144_H_
#define ADF5144_H_

#include "stm32f1xx_hal_spi.h"

SPI_HandleTypeDef hspi2;

void ad5144_write ( uint8_t cs, uint8_t pot_index, uint8_t address, uint8_t value);
uint16_t ad5144_read (uint8_t cs, uint8_t pot_index, uint8_t address, uint8_t dev);
void my_delay(int n);
#endif /* ADF5144_H_ */
