/*
 * alarm.c
 *
 *  Created on: 8 Mar 2018
 *      Author: Cilluffo Antonino
 */

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <decoder.h>
#include "alarm.h"

uint16_t bitMapDefect = 0;
volatile uint32_t ADC1ConvertedValues[14];
ADC_HandleTypeDef hadc1;



void LowSpeedManagerTask(void const * argument) {

	for (;;) {

		read_adc1();

		if (HAL_GPIO_ReadPin(LO_ALM_GPIO_Port, LO_ALM_Pin) == 0)
			bitMapDefect |= LO_ALM_DEFECT;
		else
			bitMapDefect &= ~LO_ALM_DEFECT;

		if (HAL_GPIO_ReadPin(TEC_TEMP_ALARM_GPIO_Port, TEC_TEMP_ALARM_Pin) == 0)
			bitMapDefect |= TEC_TEMP_ALARM_DEFECT;
		else
			bitMapDefect &= ~TEC_TEMP_ALARM_DEFECT;


		osDelay(1000);
	}
}

int get_defectsStatus() {

	return bitMapDefect;
}

void read_adc1()
{
	if (HAL_ADCEx_MultiModeStart_DMA(&hadc1, (uint32_t*) ADC1ConvertedValues, 14)
			!= HAL_OK) {
		while (1)
			;
	}

}


