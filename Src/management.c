/*
 * management.c
 *
 *  Created on: 5 Mar 2018
 *      Author: Cilluffo Antonino
 */

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include "usart.h"
#include "management.h"
#include "decoder.h"

uint8_t buf[64];
//static char msg[128];
SemaphoreHandle_t xConsoleSemaphore = NULL;

MESSAGE request_msg;
MESSAGE response_msg;
uint32_t checksum(MESSAGE *message);

void managerTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
		memset(buf, 0, sizeof(buf));
		uint16_t len = 0;

		int ret = HAL_UART_Receive(&huart1, (uint8_t *)&len, 2, 1000);

		if (ret == HAL_OK) {
			ret = HAL_UART_Receive(&huart1, buf, len, 1000);


			memcpy(&(request_msg.seq_num), buf, sizeof(buf));
			request_msg.msg_len = len;

			if (request_msg.end_flag == END_FLAG) {
		//		if (request_msg.csum == checksum(&request_msg)){

					response_msg.op_code = decoder(&request_msg, &response_msg);
					response_msg.seq_num = request_msg.seq_num;
					response_msg.msg_len = sizeof(response_msg ) -2;
					response_msg.end_flag = END_FLAG;

					ret = HAL_UART_Transmit(&huart1, &response_msg, len + 2, 1000);

		//		}
			}
		}
  }
  /* USER CODE END StartDefaultTask */
}


