/*
 * lookuptable.c
 *
 *  Created on: 15 May 2018
 *      Author: Cilluffo Antonino
 */

#include "lookuptable.h"

__attribute__((__section__(".lookuptable"))) const LOOK_UP_TABLE  lookup_table;
