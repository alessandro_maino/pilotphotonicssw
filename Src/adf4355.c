/*
 * ADF4355.c
 *
 *  Created on: 2 Mar 2018
 *      Author: Cilluffo Antonino
 */
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include <string.h>
#include "math.h"
#include "stdlib.h"

#include "adc.h"
#include "usart.h"
#include "spi.h"
#include "adf4355.h"

uint32_t division_factor = 0;
double division_float = 0;
uint32_t reg = 0;

#define FREQUENCY_TCXO 20000


uint32_t	reg_def[] = {	0x002015E0, 0x00000001, 0x00000012, 0x00000003,
							0x32008B84, 0x00800025, 0x35006076, 0x120000e7,
							0x102D0428, 0x05047CC9, 0x60C0067A, 0x0061300B,
							0x0001041C
};


int adf4355_reg32(int addr, uint32_t val)
{
	  int ret = HAL_OK;
	  val = (val & 0xFFFFFFF0) | addr;

	  uint8_t val_buf[4];

	  val_buf[0] = val >> 24;
	  val_buf[1] = val >> 16;
	  val_buf[2] = val >> 8;
	  val_buf[3] = val;

	  HAL_GPIO_WritePin(SPI2_CS_LO_GPIO_Port, SPI2_CS_LO_Pin, GPIO_PIN_RESET);

	  HAL_Delay(1);
	  ret =  HAL_SPI_Transmit(&hspi2 , val_buf, 4, 1000);
	  if (ret != HAL_OK) return ret;

	  HAL_Delay(1);
	  HAL_GPIO_WritePin(SPI2_CS_LO_GPIO_Port, SPI2_CS_LO_Pin, GPIO_PIN_SET);

	  return ret;
}


void ADF4355_init(){

		for (int i = 12; i >= 0; i--)
			adf4355_reg32(i, reg_def[i]);
}

void handleADF4355(char** args) {
	uint8_t value[8];
	if (strcmp(args[1], "s") == 0) {
		 parse_hex(args[2], value);

		 HAL_GPIO_WritePin(SPI2_CS_LO_GPIO_Port, SPI2_CS_LO_Pin, GPIO_PIN_RESET);
		 HAL_SPI_Transmit(&hspi2 , value, 4 ,1000);
		 HAL_GPIO_WritePin(SPI2_CS_LO_GPIO_Port, SPI2_CS_LO_Pin, GPIO_PIN_SET);
	}
}

void parse_hex(char* hex_string, uint8_t* hex_bytes) {
	char aux[3] = { 0 };
	for (int i = 0; i<strlen(hex_string)/2; i++) {
	  memcpy(aux, hex_string + 2*i, 2);
	  int value = strtol(aux, NULL, 16);
	  hex_bytes[i] = value;
	}
}

int set_frequency_khz(uint32_t frequency){

	int ret = HAL_OK;

	frequency = abs(frequency);

//	for (int i = 12; i >= 0; i--)
//		adf4355_reg32(i, reg_def[i]);


	 for (int i = 12; i >= 7; i--){
		 ret = adf4355_reg32(i, reg_def[i]);
		  if (ret != HAL_OK) return ret;
	 }


	 if (frequency <= 3400000)
		adf4355_reg32(6, 0x35206076);
	 else
		adf4355_reg32(6, reg_def[6]);



	 for (int i = 5; i >= 3; i--){
		 ret = adf4355_reg32(i, reg_def[i]);
		if (ret != HAL_OK) return ret;
	 }


	 if (frequency <= 3400000){
		 division_factor = (2*frequency)/(FREQUENCY_TCXO / 2);
		 division_float = (2*frequency)/(FREQUENCY_TCXO / 2);
	 }
	 else{
		 division_factor = (frequency)/(FREQUENCY_TCXO / 2);
		 division_float = (double) (frequency)/(FREQUENCY_TCXO / 2);

	 }
	 uint32_t mod1 = pow(2,24);
	 uint32_t frac1 = mod1 *(division_float - division_factor);
	 double frac1_float = (double) mod1 *(division_float - division_factor);

	 uint32_t frac2 = (frac1_float - frac1) *10;
	 reg = ((frac2<<18) | (0x0a << 4)) | 2;

	 ret = adf4355_reg32(2, reg);
	 if (ret != HAL_OK) return ret;

	 reg = (frac1 << 4) | 1;

	 ret = adf4355_reg32(1, reg);
	 if (ret != HAL_OK) return ret;

	 reg =  0x00200000 | (division_factor << 4);

	 ret = adf4355_reg32(0, reg);
	 if (ret != HAL_OK) return ret;

	 return ret;

}


