/*
 * AD5144.c
 *
 *  Created on: 5 Mar 2018
 *      Author: Cilluffo Antonino
 */

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

#include "adc.h"
#include "usart.h"
#include <string.h>
#include "math.h"
#include "stdlib.h"
#include "adf5144.h"


void ad5144_write( uint8_t cs, uint8_t cmd, uint8_t pot_index, uint8_t value){



	  uint8_t val = 0;

	  val = cmd | pot_index;

	  uint8_t val_buf[2];

	  val_buf[0] = val;
	  val_buf[1] = value ;

	  HAL_GPIO_WritePin(GPIOC, cs, GPIO_PIN_RESET);
	  //HAL_Delay(1);
	  HAL_SPI_Transmit(&hspi2 , val_buf, 2, 1000);
	  //HAL_Delay(1);
	  HAL_GPIO_WritePin(GPIOC, cs, GPIO_PIN_SET);
	  HAL_Delay(1);


}

uint16_t ad5144_read( uint8_t cs, uint8_t cmd, uint8_t pot_index, uint8_t dev){

	  uint8_t val = 0;
	  val =  cmd | pot_index;

	  uint8_t val_buf_t[2];
	  uint8_t val_buf_r[2];

	  val_buf_t[0] = val ;
	  val_buf_t[1] = dev;

	  HAL_GPIO_WritePin(GPIOC, cs, GPIO_PIN_RESET);
	  my_delay(10);
	  HAL_SPI_Transmit(&hspi2 , val_buf_t, 2, 100);
	  my_delay(10);
	  HAL_GPIO_WritePin(GPIOC, cs, GPIO_PIN_SET);
	  my_delay(10);

	  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;

	  HAL_GPIO_WritePin(GPIOC, cs, GPIO_PIN_RESET);
	  my_delay(10);

	  HAL_SPI_Receive(&hspi2 , val_buf_r, 2, 100);
	  my_delay(10);
	  HAL_GPIO_WritePin(GPIOC, cs, GPIO_PIN_SET);

	  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;


	  return val_buf_r[1];

}

void my_delay(int n){

	for(int i = 0; i< n; i++);
}
