/*
 * decoder.c
 *
 *  Created on: 8 Mar 2018
 *      Author: Cilluffo Antonino
 */

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include "decoder.h"
#include "flash_if.h"
#include "main.h"
#include "management.h"
#include "adf4355.h"
#include "lookuptable.h"

extern uint32_t ADC1ConvertedValues[];
uint8_t cmd, address, value;
uint8_t val= 0;
uint8_t flash_sector_aux[FLASH_PAGE_SIZE];

LOOK_UP_TABLE* lt = (LOOK_UP_TABLE*) flash_sector_aux;
//LOOK_UP_TABLE* lt;

int decoder(MESSAGE* rx_msg, MESSAGE* tx_msg){

	int ret = SET_RESPONSE;

		if (rx_msg->op_code == SET_MSG){

			switch (rx_msg->object_addr){

			case VCS_BEND_RDAC1_P1:
				lt->vcs_bend_rdac1_P1 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_1_Pin, WRITE_TO_RDAC_CMD, RDAC1_ADDR, lt->vcs_bend_rdac1_P1);

				break;

			case PWR_RF_CNTL_RDAC2_P1:
				lt->pwr_rf_cntl_rdac2_P1 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_1_Pin, WRITE_TO_RDAC_CMD, RDAC2_ADDR, lt->pwr_rf_cntl_rdac2_P1);

				break;

			case VC_MASTER_DBR_RDAC4_P1:
				lt->vc_master_dbr_rdac4_P1 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_1_Pin, WRITE_TO_RDAC_CMD, RDAC4_ADDR, lt->vc_master_dbr_rdac4_P1);

				break;

			case VC_MASTER_GAIN_RDAC1_P2:
				lt->vc_master_gain_rdac1_P2 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_2_Pin, WRITE_TO_RDAC_CMD, RDAC1_ADDR, lt->vc_master_gain_rdac1_P2);

				break;

			case LDO_CNTL_RDAC3_P2:
				lt->ldo_cntl_rdac3_P2 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_2_Pin, WRITE_TO_RDAC_CMD, RDAC3_ADDR, lt->ldo_cntl_rdac3_P2);

				break;

			case VC_SLAVE_DBR_RDAC4_P2:
				lt->vc_slave_dbr_rdac4_P2 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_2_Pin, WRITE_TO_RDAC_CMD, RDAC4_ADDR, lt->vc_slave_dbr_rdac4_P2);

				break;

			case VC_LASER2_DBR_RDAC1_P3:
				lt->vc_laser2_dbr_rda1_P3 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_3_Pin, WRITE_TO_RDAC_CMD, RDAC1_ADDR, lt->vc_laser2_dbr_rda1_P3);

				break;

			case VC_LASER1_DBR_RDAC3_P3:
				lt->vc_laser1_dbr_rda3_P3 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_3_Pin, WRITE_TO_RDAC_CMD, RDAC3_ADDR, lt->vc_laser1_dbr_rda3_P3);

				break;

			case VC_LASER1_GAIN_RDAC2_P3:
				lt->vc_laser1_gain_rda2_P3 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_3_Pin, WRITE_TO_RDAC_CMD, RDAC2_ADDR, lt->vc_laser1_gain_rda2_P3 );

				break;

			case VC_LASER2_GAIN_RDAC4_P3:
				lt->vc_laser2_gain_rda4_P3 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_3_Pin, WRITE_TO_RDAC_CMD, RDAC4_ADDR, lt->vc_laser2_gain_rda4_P3);

				break;

			case VC_LASER3_GAIN_RDAC1_P4:
				lt->vc_laser3_gain_rda1_P4 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_4_Pin, WRITE_TO_RDAC_CMD, RDAC2_ADDR, lt->vc_laser3_gain_rda1_P4);

				break;

			case VC_LASER4_GAIN_RDAC3_P4:
				lt->vc_laser4_gain_rda3_P4 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_4_Pin, WRITE_TO_RDAC_CMD, RDAC3_ADDR, lt->vc_laser4_gain_rda3_P4 );

				break;

			case VC_LASER4_DBR_RDAC2_P4:
				lt->vc_laser4_dbr_rda2_P4 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_4_Pin, WRITE_TO_RDAC_CMD, RDAC2_ADDR, lt->vc_laser4_dbr_rda2_P4);

				break;

			case VC_LASER3_DBR_RDAC4_P4:
				lt->vc_laser3_dbr_rda4_P4 = rx_msg->data_val.u8val;

				ad5144_write( SPI2_CS_POTE_4_Pin, WRITE_TO_RDAC_CMD, RDAC4_ADDR, lt->vc_laser3_dbr_rda4_P4);

				break;

			case LOOKUP_TABLE:

				FLASH_If_Init();
				FLASH_If_Erase_Sector((uint32_t) &lookup_table);
				FLASH_If_Write((uint32_t) &lookup_table, (uint32_t*) &flash_sector_aux[0], FLASH_PAGE_SIZE);

				break;

			case PLL_FREQUENCY:
				ret = set_frequency_khz(rx_msg->data_val.u32val);
				if (ret != HAL_OK) return MSG_ERROR;
				break;

			}

			return SET_RESPONSE;

		}
		else if (rx_msg->op_code == GET_MSG){

			switch (rx_msg->object_addr){

			case I_LASER1_GAIN:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER1_GAIN_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_LASER2_GAIN:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER2_GAIN_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_LASER3_GAIN:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER3_GAIN_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_LASER4_GAIN:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER4_GAIN_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_LASER1_DBR:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER1_DBR_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_LASER2_DBR:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER2_DBR_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_LASER3_DBR:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER3_DBR_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_LASER4_DBR:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_LASER4_DBR_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case TEC_TEMP:
				tx_msg->data_val.u32val = ADC1ConvertedValues[TEC_TEMP_ALARM_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case PWR_DET:
				tx_msg->data_val.u32val = ADC1ConvertedValues[PWR_DECT_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_MASTER_GAIN:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_MASTER_GAIN_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_SLAVE_DBR:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_SLAVE_DBR_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_MASTER_DBR:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_MASTER_DBR_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case I_S_BEND:
				tx_msg->data_val.u32val = ADC1ConvertedValues[I_S_BEND_ADC];
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT32;
				break;

			case VCS_BEND_RDAC1_P1:


				//ad5144_write( SPI2_CS_POTE_1_Pin, 0x10, RDAC1_ADDR, 0x70);
				HAL_Delay(10);
				val = ad5144_read( SPI2_CS_POTE_1_Pin, READ_BACK_TO_RDAC_CMD, RDAC1_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case PWR_RF_CNTL_RDAC2_P1:

				val = ad5144_read( SPI2_CS_POTE_1_Pin, READ_BACK_TO_RDAC_CMD, RDAC2_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_MASTER_DBR_RDAC4_P1:

				val = ad5144_read( SPI2_CS_POTE_1_Pin, READ_BACK_TO_RDAC_CMD, RDAC4_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_MASTER_GAIN_RDAC1_P2:

				val = ad5144_read( SPI2_CS_POTE_2_Pin, READ_BACK_TO_RDAC_CMD, RDAC1_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case LDO_CNTL_RDAC3_P2:

				val = ad5144_read( SPI2_CS_POTE_2_Pin, READ_BACK_TO_RDAC_CMD, RDAC3_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_SLAVE_DBR_RDAC4_P2:

				val = ad5144_read( SPI2_CS_POTE_2_Pin, READ_BACK_TO_RDAC_CMD, RDAC4_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER2_DBR_RDAC1_P3:

				val = ad5144_read( SPI2_CS_POTE_3_Pin, READ_BACK_TO_RDAC_CMD, RDAC1_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER1_DBR_RDAC3_P3:

				val = ad5144_read( SPI2_CS_POTE_3_Pin, READ_BACK_TO_RDAC_CMD, RDAC3_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER1_GAIN_RDAC2_P3:

				val = ad5144_read( SPI2_CS_POTE_3_Pin, READ_BACK_TO_RDAC_CMD, RDAC2_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER2_GAIN_RDAC4_P3:

				val = ad5144_read( SPI2_CS_POTE_3_Pin, READ_BACK_TO_RDAC_CMD, RDAC4_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER3_GAIN_RDAC1_P4:

				val = ad5144_read( SPI2_CS_POTE_4_Pin, READ_BACK_TO_RDAC_CMD, RDAC1_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER4_GAIN_RDAC3_P4:

				val = ad5144_read( SPI2_CS_POTE_4_Pin, READ_BACK_TO_RDAC_CMD, RDAC3_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER4_DBR_RDAC2_P4:

				val = ad5144_read( SPI2_CS_POTE_4_Pin, READ_BACK_TO_RDAC_CMD, RDAC2_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;

			case VC_LASER3_DBR_RDAC4_P4:

				val = ad5144_read( SPI2_CS_POTE_4_Pin, READ_BACK_TO_RDAC_CMD, RDAC4_ADDR, 0x03);

				tx_msg->data_val.u8val = val;
				tx_msg->data_len = 4;
				tx_msg->data_type = UINT8;

				break;



			}

			return GET_RESPONSE;

		}
		else
			return MSG_ERROR;


}


uint32_t checksum(MESSAGE *message)
{
    int i;
    int chk=0;
    unsigned char *data;

    data = (unsigned char *)message;
    for (i=0; i < sizeof(MESSAGE) -4; i++) chk += *data++;
    return chk;
}
